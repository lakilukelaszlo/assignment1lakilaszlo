package RPGAssingmentLaki.Hero;

public class PrimaryAttribute
{
    //PrimaryAttributes
    public int strength;
    public int dexterity;
    public int intelligence;
    public int level;

    //Primary Attributes Getters/Setters
    public int getStrength() {
        return strength;
    }

    public void setStrength(int strength) {
        this.strength = strength;
    }

    public int getDexterity() {
        return dexterity;
    }

    public void setDexterity(int dexterity) {
        this.dexterity = dexterity;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    public int getLevel() {
        return level;
    }


    public void setIntelligence(int intelligence) {
        this.intelligence = intelligence;
    }

    public int getIntelligence() {
        return intelligence;
    }

}