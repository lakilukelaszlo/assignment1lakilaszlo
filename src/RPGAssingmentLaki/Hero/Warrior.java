package RPGAssingmentLaki.Hero;

import RPGAssingmentLaki.Item.Armors;
import RPGAssingmentLaki.Item.Weapons;
import RPGAssingmentLaki.Methods.Methods;

import java.util.ArrayList;

public class Warrior extends Hero implements Methods {

    //Warrior Constructor
    public Warrior(String name, int level, int strength, int dexterity, int intelligence, int damage) {
        super(name, level);
        //PrimaryAttribute baseAttributes = new PrimaryAttribute();
        this.strength = strength;
        this.dexterity = strength;
        this.intelligence = strength;
        this.level=level;
        this.damage = damage;


    }

    public Warrior() {

    }

    //Warrior Getter/Setter
    @Override
    public void setName(String name) {
        super.setName("Warrior");
    }
    @Override
    public void setLevel(int level) {
        super.setLevel(1);
    }

    @Override
    public void setStrength(int strength) {
        super.setStrength(5);
    }

    @Override
    public void setDexterity(int dexterity) {
        super.setDexterity(2);
    }

    @Override
    public void setIntelligence(int intelligence) {
        super.setIntelligence(1);
    }
    //Warrior Methods
    @Override
    public void Attack() {
        System.out.println("Warrior Attacks!!");
    }

    @Override
    public int LevelUp() {
        this.level++;
        this.strength+=3;
        this.dexterity+=2;
        this.intelligence+=1;
        return level;
    }
    @Override
    public void dealDamage(){}

    @Override
    public boolean canEquipWeapon(String characterName, String weaponName) {
        boolean canWarriorEquip = false;

            if (characterName == "Warrior" && weaponName == "axes" || weaponName == "hammers" || weaponName == "swords") {
                canWarriorEquip = true;
            } else {
                canWarriorEquip = false;
            }
        return canWarriorEquip;
    }
    @Override
    public boolean canEquipArmor(String characterName, String armorName){
        boolean canWarriorEquip=false;

            if(characterName=="Warrior" && armorName=="plate" || armorName=="mail"){
                canWarriorEquip=true;
            }
            else {
                canWarriorEquip=false;
            }

        return canWarriorEquip;
    }
    @Override
    public int DPS(int attackSpeed, int damage){
        return attackSpeed * damage;
    }

}
