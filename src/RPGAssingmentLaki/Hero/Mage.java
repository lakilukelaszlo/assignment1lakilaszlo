package RPGAssingmentLaki.Hero;

import RPGAssingmentLaki.Item.Armors;
import RPGAssingmentLaki.Item.Weapons;
import RPGAssingmentLaki.Methods.Methods;

import java.util.ArrayList;

public class Mage extends Hero implements Methods {
    //Mage Attributes
    private  int damage;



    //Mage Getters/Setters
    public int getDamage() {
        return damage;
    }

    public void setDamage(int damage) {
        this.damage = damage;
    }

    //Mage Constructors
    public Mage() {
        super();
    }

    public Mage(String name, int level, int damage) {
        super(name, level);
        this.damage = damage;
    }
    public Mage(String name, int level, int strength, int dexterity, int intelligence, int damage) {
        super(name, level);
        //PrimaryAttribute baseAttributes = new PrimaryAttribute();
        this.strength = strength;
        this.dexterity = strength;
        this.intelligence = strength;
        this.level=level;
        this.damage = damage;
    }
    //Mage Methods
    @Override
    public void Attack() {
        System.out.println("Mage Attacks!!");
    }

    @Override
    public int LevelUp() {

        this.strength+=1;
        this.dexterity+=1;
        this.intelligence+=5;
        this.level+=2;
        return  level;
    }
    @Override
    public void dealDamage(){}

    @Override
    public boolean canEquipWeapon(String characterName, String weaponName){
        boolean canMageEquip=false;

            if(characterName=="Mage" && weaponName =="staffs" || weaponName=="wands" ){
                canMageEquip=true;
            }
            else {
                canMageEquip=false;
            }

        return canMageEquip;
    }

    @Override
    public boolean canEquipArmor(String characterName, String armorName){
        boolean canMageEquip=false;

            if(characterName=="Mage" && armorName=="cloth") {
                canMageEquip = true;
            }
            else {
                canMageEquip=false;
            }

        return canMageEquip;
    }
    @Override
    public int DPS(int attackSpeed, int damage){
        return attackSpeed * damage;
    }

}