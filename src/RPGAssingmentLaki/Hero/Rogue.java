package RPGAssingmentLaki.Hero;

import RPGAssingmentLaki.Item.Armors;
import RPGAssingmentLaki.Item.Weapons;
import RPGAssingmentLaki.Methods.Methods;

import java.util.ArrayList;

public class Rogue extends Hero implements Methods {
    //Rogue Constructors
    public Rogue() {

    }
    public Rogue(String name, int level, int strength, int dexterity, int intelligence, int damage) {
        super(name, level);
        //PrimaryAttribute baseAttributes = new PrimaryAttribute();
        this.strength = strength;
        this.dexterity = strength;
        this.intelligence = strength;
        this.level=level;
        this.damage = damage;

    }

    //Rogue Getters/setters
    @Override
    public void setName(String name) {
        super.setName("Rogue");
    }
    @Override
    public void setLevel(int level) {
        super.setLevel(1);
    }

    @Override
    public void setStrength(int strength) {
        super.setStrength(2);
    }

    @Override
    public void setDexterity(int dexterity) {
        super.setDexterity(6);
    }

    @Override
    public void setIntelligence(int intelligence) {
        super.setIntelligence(1);
    }
    //Rogue Methods
    @Override
    public void Attack() {
        System.out.println("Rouge Attacks!!");
    }

    @Override
    public int LevelUp() {
        this.level++;
        this.strength+=1;
        this.dexterity+=4;
        this.intelligence+=1;
        return level;
    }
    @Override
    public void dealDamage(){}

    @Override
    public boolean canEquipWeapon(String characterName,String weaponName){
        boolean canRogueEquip=false;
            if(characterName=="Rogue" && weaponName=="daggers" || weaponName=="swords" ){
                canRogueEquip=true;
            }
            else {
                canRogueEquip=false;
            }

        return canRogueEquip;
    }
    @Override
    public boolean canEquipArmor(String characterName, String armorName){
        boolean canRogueEquip=false;

            if(characterName=="Rogue" && armorName=="leather" || armorName=="mail"){
                canRogueEquip=true;
            }
            else {
                canRogueEquip=false;
            }

        return canRogueEquip;
    }
    @Override
    public int DPS(int attackSpeed, int damage){
        return attackSpeed * damage;
    }
}