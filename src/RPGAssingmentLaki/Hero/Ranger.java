package RPGAssingmentLaki.Hero;

import RPGAssingmentLaki.Item.Armors;
import RPGAssingmentLaki.Item.Weapons;
import RPGAssingmentLaki.Methods.Methods;

import java.util.ArrayList;

public class Ranger extends Hero implements Methods {
    //Ranger Constructor
    public Ranger(String name, int level, int strength, int dexterity, int intelligence, int damage) {
        super(name, level);
        //PrimaryAttribute baseAttributes = new PrimaryAttribute();
        this.strength = strength;
        this.dexterity = strength;
        this.intelligence = strength;
        this.level=level;
        this.damage = damage;

    }

    public Ranger() {

    }

    @Override
    public void setName(String name) {
        super.setName("Ranger");
    }
    @Override
    public void setLevel(int level) {
        super.setLevel(1);
    }

    @Override
    public void setStrength(int strength) {
        super.setStrength(1);
    }

    @Override
    public void setDexterity(int dexterity) {
        super.setDexterity(7);
    }

    @Override
    public void setIntelligence(int intelligence) {
        super.setIntelligence(1);
    }

    @Override
    public void Attack() {
        System.out.println("Ranger Attacks!!");
    }

    @Override
    public int LevelUp() {
        this.level++;
        this.strength+=1;
        this.dexterity+=5;
        this.intelligence+=1;
        return level;
    }
    @Override
    public void dealDamage(){}

    @Override
    public boolean canEquipWeapon(String characterName, String weaponName){
        boolean canRangerEquip=false;

            if(characterName=="Ranger" && weaponName=="bows"){
                canRangerEquip=true;
            }
            else {
                canRangerEquip=false;
            }
        return canRangerEquip;
    }

    @Override
    public boolean canEquipArmor(String characterName, String armorName){
        boolean canRangerEquip=false;

            if(characterName=="Ranger" && armorName =="leather" || armorName =="mail"){
                canRangerEquip=true;
            }
            else {
                canRangerEquip=false;
            }
        return canRangerEquip;
    }
    @Override
    public int DPS(int attackSpeed, int damage){
        return attackSpeed * damage;
    }
}