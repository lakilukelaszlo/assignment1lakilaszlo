package RPGAssingmentLaki.Hero;

public abstract class Hero extends PrimaryAttribute {
    //Hero Attributes
    protected int damage;
    private String name;

    private PrimaryAttribute baseAttributes;
    private PrimaryAttribute totalAttributes;

    //Hero Constructors
    public Hero() {}

    public Hero(String name, int level) {
        this.name = name;
        this.level = level;
    }

    public Hero(String name, int level, int strength, int dexterity, int intelligence) {
        this.name = name;

        baseAttributes = new PrimaryAttribute();
        baseAttributes.strength = strength;
        baseAttributes.dexterity = dexterity;
        baseAttributes.intelligence = intelligence;
        baseAttributes.level=level;
    }

    //Hero Getters/Setters
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    public PrimaryAttribute getBaseAttributes() {
        return baseAttributes;
    }

    public void setBaseAttributes(PrimaryAttribute baseAttributes) {
        this.baseAttributes = baseAttributes;
    }

    public PrimaryAttribute getTotalAttributes() {
        return totalAttributes;
    }

    public void setTotalAttributes(PrimaryAttribute totalAttributes) {
        this.totalAttributes = totalAttributes;
    }
}