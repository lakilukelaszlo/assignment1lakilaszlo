package RPGAssingmentLaki.Methods;

import RPGAssingmentLaki.Hero.PrimaryAttribute;
import RPGAssingmentLaki.Item.Armors;
import RPGAssingmentLaki.Item.Weapons;

import java.util.ArrayList;

public interface Methods {
    /**
* Returns an Image object that can then be painted on the screen. 

* that draw the image will incrementally paint on the screen. 
*
* @param  url  an absolute URL giving the base location of the image
* @param  name the location of the image, relative to the url argument
* @return      the image at the specified URL
* @see         Image
*/
    public void Attack();
    public int LevelUp();
    public void dealDamage();
    public boolean canEquipWeapon(String characterName, String weaponName);
    public boolean canEquipArmor(String characterName, String armorName);
    public int DPS(int attackSpeed, int damage);



}
