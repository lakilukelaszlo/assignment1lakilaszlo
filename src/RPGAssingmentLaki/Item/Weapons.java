package RPGAssingmentLaki.Item;

public class Weapons extends Item {
    //Weapon Attributes
    private String axes;
    private String bows;
    private String daggers;
    private String hammers;
    private String staffs;
    private String swords;
    private String wands;
    private int damage;
    private int attackpersec;




    //Weapon Constructors
    public Weapons(String name, int requiredLevel, String slot, int damage) {
        super(name, requiredLevel);
    }

    public Weapons(String name, int requiredLevel, String slot, String axes, String bows, String daggers,
                   String hammers, String staffs, String swords, String wands, int damage,int attackpersec) {
        super(name, requiredLevel);
        this.axes = axes;
        this.bows = bows;
        this.daggers = daggers;
        this.hammers = hammers;
        this.staffs = staffs;
        this.swords = swords;
        this.wands = wands;
        this.damage=damage;
        this.attackpersec=attackpersec;
    }

}