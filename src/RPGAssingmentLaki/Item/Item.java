package RPGAssingmentLaki.Item;

public abstract class Item {
    //Item Attributes
    private String name;
    private int requiredLevel;

    private int damage;
    //Item Getters/setters
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getRequiredLevel() {
        return requiredLevel;
    }

    public void setRequiredLevel(int requiredLevel) {
        this.requiredLevel = requiredLevel;
    }

    public int getDamage() {
        return damage;
    }

    public void setDamage(int damage) {
        this.damage = damage;
    }
    //Item Constructor
    public Item(String name, int requiredLevel) {
        this.name = name;
        this.requiredLevel = requiredLevel;
        this.damage=damage;
    }
}