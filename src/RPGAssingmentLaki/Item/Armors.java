package RPGAssingmentLaki.Item;

public class Armors extends Item {
    //Armors Attributes
    private String cloth;
    private String leather;
    private String  mail;
    private String  plate;

    //Armors Getters/Setters

    public String getCloth() {
        return cloth;
    }

    public void setCloth(String cloth) {
        this.cloth = cloth;
    }

    public String getLeather() {
        return leather;
    }

    public void setLeather(String leather) {
        this.leather = leather;
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public String getPlate() {
        return plate;
    }

    public void setPlate(String plate) {
        this.plate = plate;
    }


    //Armor Constructor

    public Armors(String name, int requiredLevel, String slot, String cloth, String leather, String mail, String plate) {
        super(name, requiredLevel);
        this.cloth = cloth;
        this.leather = leather;
        this.mail = mail;
        this.plate = plate;
    }
}