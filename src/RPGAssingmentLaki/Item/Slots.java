package RPGAssingmentLaki.Item;

import java.util.HashMap;

public class Slots {
    //slot Attributes
    public String headSlot;
    public String bodySlot;
    public String legsSlot;
    public String weaponSlot;
    //Slots Constructor
    public Slots(String headSlot, String bodySlot, String legsSlot, String weaponSlot) {
        this.headSlot = headSlot;
        this.bodySlot = bodySlot;
        this.legsSlot = legsSlot;
        this.weaponSlot = weaponSlot;
    }
    HashMap<Slots, Item> equipment=new HashMap<Slots,Item>();

}
