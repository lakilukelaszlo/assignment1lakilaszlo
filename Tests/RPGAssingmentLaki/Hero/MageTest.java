package RPGAssingmentLaki.Hero;

import RPGAssingmentLaki.Item.Armors;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.*;

class MageTest {
    @Test
    void levelUp_should_return_plus_one() {
        //Arrange
        Mage mage=new Mage();
        int expected=1+1;
        //Act
        int actual=mage.LevelUp();
        //Assert
        assertEquals(expected,actual);
    }
    @Test
    void equip_Valid_Armor_Should_return_true(){
        //Arrange
        Mage mage=new Mage();
        boolean isArmorValid;
        boolean expected=true;
        //Act
        boolean actual=mage.canEquipArmor("Mage","cloth");
        //Assert
        assertEquals(expected,actual);
    }
    @Test
    void equip_Valid_Weapon_Should_return_true(){
        //Arrange
        Mage mage=new Mage();
        boolean isArmorValid;
        boolean expected=true;
        //Act
        boolean actual=mage.canEquipWeapon("Mage","staffs");
        //Assert
        assertEquals(expected,actual);
    }
}