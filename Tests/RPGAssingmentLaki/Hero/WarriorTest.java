package RPGAssingmentLaki.Hero;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class WarriorTest {

    @Test
    void levelUp_should_return_plus_one() {
        //Arrange
        Warrior warrior=new Warrior();
        int expected=1+1;
        //Act
        int actual=warrior.LevelUp();
        //Assert
        assertEquals(expected,actual);
    }
    @Test
    void equip_Valid_Armor_Should_return_true(){
        //Arrange
        Warrior warrior=new Warrior();
        boolean isArmorValid;
        boolean expected=true;
        //Act
        boolean actual=warrior.canEquipArmor("Warrior","plate");
        //Assert
        assertEquals(expected,actual);
    }
    @Test
    void equip_Valid_Weapon_Should_return_true(){
        //Arrange
        Warrior warrior=new Warrior();
        boolean isArmorValid;
        boolean expected=true;
        //Act
        boolean actual=warrior.canEquipWeapon("Warrior","hammers");
        //Assert
        assertEquals(expected,actual);
    }

}