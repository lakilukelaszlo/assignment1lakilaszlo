package RPGAssingmentLaki.Hero;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class RangerTest {

    @Test
    void levelUp_should_return_plus_one() {
        //Arrange
        Ranger ranger=new Ranger();
        int expected=1+1;
        //Act
        int actual=ranger.LevelUp();
        //Assert
        assertEquals(expected,actual);
    }
    @Test
    void equip_Valid_Armor_Should_return_true(){
        //Arrange
        Ranger ranger=new Ranger();
        boolean isArmorValid;
        boolean expected=true;
        //Act
        boolean actual=ranger.canEquipArmor("Ranger","leather");
        //Assert
        assertEquals(expected,actual);
    }
    @Test
    void equip_Valid_Weapon_Should_return_true(){
        //Arrange
        Ranger ranger=new Ranger();
        boolean isArmorValid;
        boolean expected=true;
        //Act
        boolean actual=ranger.canEquipWeapon("Ranger","bows");
        //Assert
        assertEquals(expected,actual);
    }
}