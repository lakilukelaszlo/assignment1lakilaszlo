package RPGAssingmentLaki.Hero;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class RogueTest {

    @Test
    void levelUp_should_return_plus_one() {

            //Arrange
            Rogue rogue=new Rogue();
            int expected=1+1;
            //Act
            int actual=rogue.LevelUp();
            //Assert
            assertEquals(expected,actual);
    }
    @Test
    void equip_Valid_Armor_Should_return_true(){
        //Arrange
        Rogue rogue=new Rogue();
        boolean isArmorValid;
        boolean expected=true;
        //Act
        boolean actual=rogue.canEquipArmor("Rouge","mail");
        //Assert
        assertEquals(expected,actual);
    }
    @Test
    void equip_Valid_Weapon_Should_return_true(){
        //Arrange
        Rogue rogue=new Rogue();
        boolean isArmorValid;
        boolean expected=true;
        //Act
        boolean actual=rogue.canEquipWeapon("Rouge","swords");
        //Assert
        assertEquals(expected,actual);
    }
}