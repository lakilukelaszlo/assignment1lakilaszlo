﻿# RPG CHARACTERS

[![standard-readme compliant](https://img.shields.io/badge/standard--readme-OK-green.svg?style=flat-square)](https://github.com/RichardLitt/standard-readme)

## Table of Contents

- [Background](#background)
- [Install](#install)
- [Usage](#usage)
- [Maintainers](#maintainers)
- [Contributing](#contributing)
- [License](#license)

## Background

Role playing games are games in which players advance through a story quest, and often many side quests, for which their character or party of characters gain experience that improves various attributes and abilities.

In this game there are fou classes:

- Mage
- Warrior
- Ranger
- Rouge

These classes can equip armors:

- Cloth
- Leather
- Mail
- Plate

They can also equip weapons:

- Axes
- Bows
- Daggers
- Hammers
- Staffs
- Leather
- Swords
- Wands

## Install

- Install JDK 17
- Install Intellij
- Clone repository

## Usage

- Run the application
  - Make sure to download monthly data and fill in any blanks. The Apache POI library doesnt seem to replace blanks when it should.
- Place in `resources` folder to be searchable by the readers.

## Maintainers

[@LakiLászló](https://gitlab.com/lakilukelaszlo)

## Contributing

PRs accepted.

Small note: If editing the README, please conform to the [standard-readme](https://github.com/RichardLitt/standard-readme) specification.

## License


